var User = require('models/user').User;
var HttpError = require('error').HttpError;
var RegError = require('models/user').RegError;
var async = require('async');


exports.get = function(req, res) {
  res.locals.user_id = req.session.user_id;
  res.render('registration');
};

exports.post = function(req, res, next) {
  var username = req.body.username;
  var pwd = req.body.pwd;
  var repeatPwd = req.body.repeatPwd;

  if (username == '' || pwd == '' || repeatPwd == '')
    return next(new HttpError(403, "Все поля должны быть заполнены!"));
  // console.log(username, pwd, repeatPwd);

  User.registration(username, pwd, repeatPwd, function(err, user) {
    if(err) {
      if(err instanceof RegError) {
        console.log(err);
        return next(new HttpError(403, err.message));
      } else {
        return next(err);
      }
    }
    req.session.user_id = user._id;
    res.send({});
  });
};