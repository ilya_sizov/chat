var express = require('express');
var http = require('http');
var path = require('path');
// var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var conf = require('config');
var log = require('libs/log')(module);
var HttpError = require('error').HttpError;
var mongoose = require('libs/mongoose');

var app = express();
// app.use(favicon('./public/images/favicon.ico'));

app.set('port', process.env.PORT || conf.get('port'));

// Указываем в какой папке шаблоны странц
app.set('views', path.join(__dirname, 'template'));
//Указываем что фалй html обрабатывать не обычным ejs, а ejs-locals двжком
app.engine('html', require('ejs-locals'));
app.set('view engine', 'html');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var MongoStore = require('connect-mongo')(session);

app.use(session({
  secret: conf.get('session:secret'),
  key: conf.get('session:key'),
  cookie: conf.get('session:cookie'),
  store: new MongoStore({mongoose_connection: mongoose.connection})
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use(require('libs/sendHttpError'));
app.use(require('libs/loadUser'));

//Подключаем маршурты
require('routes')(app);

// Обработка ошибок
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  if (typeof err == 'number') { // next(404);
    err = new HttpError(err);
  }

  if (err instanceof HttpError) {
    res.sendHttpError(err);
  } else {
    if (app.get('env') == 'development') {
      console.log(err.status);
      res.status(err.status || 500);
      res.render('error.html', {
        message: err.message,
        error: err
      });
    } else {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: {}
      });
    }
  }
});

var server = http.createServer(app).listen(app.get('port'), function(){
  log.info('Express server listening on port ' + app.get('port'));
});

//Подключаем сокеты
require('socket')(server);