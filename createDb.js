var mongoose = require('libs/mongoose');
var async = require('async');
var User = require('models/user').User;

async.series([
	open,
	dropDatabase,
	requireModels,
	createUsers
	], function(err, results) {
		mongoose.disconnect();
		console.log(arguments);
	});

function open(callback) {
	mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
	var db = mongoose.connection.db;
	db.dropDatabase(callback);
}

function requireModels(callback) {
	var User = require('models/user').User;

	async.each(Object.keys(mongoose.models), function(modelName, callback) {
		mongoose.models[modelName].ensureIndexes(callback);
	}, 
	callback); 
}

function createUsers(callback) {
	

	var users = [
		{username: 'Вася', password: 'supervasya'},
		{username: 'Вася1111', password: '123'},
		{username: 'admin', password: 'admin'}
	];

	async.each(users, function(userData, callback) {
		var user = new User(userData);
		user.save(callback);
	}, 
	callback); 
}

function close(callback){
	mongoose.disconnect(callback);
}

