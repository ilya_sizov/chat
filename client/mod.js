var Mod = {};

Mod.preloader = function(show) {
  if (show) {
    if ($('#preloader').hasClass('hide'))
      $('#preloader').removeClass('hide');
  } else {
    if (!$('#preloader').hasClass('hide'))
      $('#preloader').addClass('hide');
  }
}

Mod.ajaxpost = function(url, parameters, donecallback, preloader, errorBoxName) {
		if(preloader) Mod.preloader(true);

    $.ajax({
      type: "POST",
      url: url,
      data: parameters,
      success: function(data) {
        donecallback(data);
      }
    }).fail(function(error) {
    	console.log(error);
      var err = error.responseJSON;
    	if (errorBoxName) $('#'+errorBoxName).html(err.message).removeClass("hide").addClass("alert-danger");
    }).always(function() {
      if (preloader) Mod.preloader(false);
    });
  }

  Mod.error = function(text) {
    setTimeout(function() {
      swal({
        title: 'Ошибка!',
        text: text,
        type: "error"/*,
        confirmButtonColor: "#DD6B55"*/
      });
    }, 100);
  };

  Mod.success = function(text, afterScript) {
    setTimeout(function() {
  		swal({
				title: 'Выполнено!',
      	text: text,
      	type: "success"/*,
      	confirmButtonColor: "#68b83a"*/
			}, afterScript);
    }, 100);
  };

  Mod.confirm = function(title, text, callbackyes) {
    swal({
      title: title,
      text: text,
      type: "warning",
      showCancelButton: true,
      /*confirmButtonColor: "#DD6B55", */
      confirmButtonText: "Да",
      cancelButtonText: "Нет"
    },
    function(){
      callbackyes();
    });
  };




module.exports = window.Mod = Mod;