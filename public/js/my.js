$(document).ready(function() {

	$('#btnLogin').click(function() {
		if (!$('#msgBox').hasClass("hide"))
			$('#msgBox').addClass("hide");

	});

	$('#btnLogout').click(function() {
		Mod.ajaxpost('/logout', {}, function(data) {
			window.location.href = "/";
		}, true);
	});


	$('#btnSignin').click(function(){
		var data = {username:$('#login').val(), password:$('#pwd').val() };

		Mod.ajaxpost('/login', data, function(data) {
			Mod.success("Вход выполнен!", function() {
				window.location.href = "/chat";
			});
		},
		true,
		"msgBox");
  });

	$('#btnReg').click(function(){
		var data = {
			username: $('#newLogin').val(),
			pwd: $('#newPwd').val(),
			repeatPwd: $('#repeatPwd').val()
		};

		Mod.ajaxpost('/registration', data, function(data) {
			Mod.success("Пользователь зарегистрирован!", function() {
				window.location.href = "/chat";
			});
		},
		true,
		"errorBox");
  });


});