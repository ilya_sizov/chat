(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var Mod = {};

Mod.preloader = function(show) {
  if (show) {
    if ($('#preloader').hasClass('hide'))
      $('#preloader').removeClass('hide');
  } else {
    if (!$('#preloader').hasClass('hide'))
      $('#preloader').addClass('hide');
  }
}

Mod.ajaxpost = function(url, parameters, donecallback, preloader, errorBoxName) {
		if(preloader) Mod.preloader(true);

    $.ajax({
      type: "POST",
      url: url,
      data: parameters,
      success: function(data) {
        donecallback(data);
      }
    }).fail(function(error) {
    	console.log(error);
      var err = error.responseJSON;
    	if (errorBoxName) $('#'+errorBoxName).html(err.message).removeClass("hide").addClass("alert-danger");
    }).always(function() {
      if (preloader) Mod.preloader(false);
    });
  }

  Mod.error = function(text) {
    setTimeout(function() {
      swal({
        title: 'Ошибка!',
        text: text,
        type: "error"/*,
        confirmButtonColor: "#DD6B55"*/
      });
    }, 100);
  };

  Mod.success = function(text, afterScript) {
    setTimeout(function() {
  		swal({
				title: 'Выполнено!',
      	text: text,
      	type: "success"/*,
      	confirmButtonColor: "#68b83a"*/
			}, afterScript);
    }, 100);
  };

  Mod.confirm = function(title, text, callbackyes) {
    swal({
      title: title,
      text: text,
      type: "warning",
      showCancelButton: true,
      /*confirmButtonColor: "#DD6B55", */
      confirmButtonText: "Да",
      cancelButtonText: "Нет"
    },
    function(){
      callbackyes();
    });
  };




module.exports = window.Mod = Mod;
},{}]},{},[1]);
