var browserify = require('browserify'),
    watchify = require('watchify'),
    gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    streamify = require('gulp-streamify'),
    source = require('vinyl-source-stream'),
    sourceFile = './client/mod.js',
    destFolder = './public/build/',
    destFile = 'mod.js';

gulp.task('browserify', function() {
  return browserify(sourceFile)
  .bundle()
  .pipe(source(destFile))
  // .pipe(streamify(uglify()))
  .pipe(gulp.dest(destFolder));
});



// gulp.task('default', ['browserify']);


gulp.task('default', function() {
    gulp.run('browserify');

    gulp.watch('./client/*.js', function(event) {
        gulp.run('browserify');
    })
})